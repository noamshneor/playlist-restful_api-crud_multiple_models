import { ISong } from "../song/song.interface.js";

export interface IPlaylist {
    id?: string;
    name: string;
    songs?: ISong[];
}
