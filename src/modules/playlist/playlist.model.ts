import mongoose from "mongoose";
const { Schema, model } = mongoose;

const PlaylistSchema = new Schema({
    name: String,
    songs: [{ type: Schema.Types.ObjectId, ref: "song" }],
});

export default model("playlist", PlaylistSchema);
