import { IPlaylist } from "./playlist.interface.js";
import {
    newPlaylistDB,
    addSongToPlaylistDB,
    getAllPlaylistsDB,
    getPlaylistDB,
    updatePlaylistDB,
    deletePlaylistDB,
    getPaginatePlaylistDB,
} from "./playlist.repository.js";

export async function newPlaylist(playlist: IPlaylist) {
    return await newPlaylistDB(playlist);
}

export async function addSongToPlaylist(playlist_id: string, song_id: string) {
    return await addSongToPlaylistDB(playlist_id, song_id);
}

export async function getAllPlaylists() {
    return await getAllPlaylistsDB();
}

export async function getPlaylist(id: string) {
    return await getPlaylistDB(id);
}

export async function updatePlaylist(id: string, payload: IPlaylist) {
    return await updatePlaylistDB(id, payload);
}

export async function deletePlaylist(id: string) {
    return await deletePlaylistDB(id);
}

export async function getPaginate(pageNum: string, nPerPage: string) {
    const pNum = parseInt(pageNum);
    const nPPage = parseInt(nPerPage);
    return await getPaginatePlaylistDB(pNum, nPPage);
}
