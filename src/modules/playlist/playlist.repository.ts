import playlist_model from "./playlist.model.js";
import song_model from "../song/song.model.js";
import { IPlaylist } from "./playlist.interface.js";

export async function newPlaylistDB(playlist: IPlaylist) {
    return await playlist_model.create(playlist);
}

export async function addSongToPlaylistDB(
    playlist_id: string,
    song_id: string
) {
    const playlist = await playlist_model.findById(playlist_id);
    const song = await song_model.findById(song_id);

    if (!playlist.songs.includes(song_id)) {
        playlist.songs.push(song);
        await playlist.save();
    }
    if (!song.playlists.includes(playlist_id)) {
        song.playlists.push(playlist);
        await song.save();
    }
    return playlist;
}

export async function getAllPlaylistsDB() {
    return await playlist_model.find().select();
}

export async function getPlaylistDB(id: string) {
    return await playlist_model.findById(id).populate("songs");
}

export async function updatePlaylistDB(
    id: string,
    payload: IPlaylist
) {
    return await playlist_model.findByIdAndUpdate(id, payload, {
        new: true,
        upsert: false,
    });
}

export async function deletePlaylistDB(id: string) {
    const playlist = await playlist_model.findByIdAndRemove(id);
    for (const songID of playlist.songs) {
        const song = await song_model.findById(songID);
        const index = song.playlists.indexOf(songID);
        if (index > -1) {
            song.playlists.splice(index, 1);
        }
        await song.save();
    }
    return playlist;
}

export async function getPaginatePlaylistDB(pageNum: number, nPerPage: number) {
    return await playlist_model
        .find()
        .skip(pageNum > 0 ? (pageNum - 1) * nPerPage : 0)
        .limit(nPerPage)
        .select();
}
