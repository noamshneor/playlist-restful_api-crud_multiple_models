/* 
  if there is an error thrown in the DB, asyncMiddleware
  will pass it to next() and express will handle the error */
import raw from "../../middleware/route.async.wrapper.js";
import express, { Request, Response, NextFunction } from "express";
import schema from "./playlist.validator.js";
import {
    newPlaylist,
    addSongToPlaylist,
    getAllPlaylists,
    getPlaylist,
    updatePlaylist,
    deletePlaylist,
    getPaginate,
} from "./playlist.service.js";

const router = express.Router();

// parse json req.body on post routes
router.use(express.json());

// CREATES A NEW PLAYLIST
router.post(
    "/",
    raw(async (req: Request, res: Response, next: NextFunction) => {
        await schema.validateAsync(req.body);
        next();
    }),
    raw(async (req: Request, res: Response) => {
        const playlist = await newPlaylist(req.body);
        res.status(200).json(playlist);
    })
);

// ADD SONG TO PLAYLIST
router.post(
    "/:playlist_id/song/:song_id",
    raw(async (req, res) => {
        const { playlist_id, song_id } = req.params;
        const playlist = await addSongToPlaylist(playlist_id, song_id);
        res.status(200).json(playlist);
    })
);

// GET ALL PLAYLISTS
router.get(
    "/",
    raw(async (req: Request, res: Response) => {
        const playlists = await getAllPlaylists();
        res.status(200).json(playlists);
    })
);

// GETS A SINGLE PLAYLIST
router.get(
    "/:id",
    raw(async (req: Request, res: Response) => {
        const playlist = await getPlaylist(req.params.id);
        if (!playlist)
            return res.status(404).json({ status: "No playlist found." });
        res.status(200).json(playlist);
    })
);

// UPDATES A SINGLE PLAYLIST
router.put(
    "/:id",
    raw(async (req: Request, res: Response, next: NextFunction) => {
        await schema.validateAsync(req.body);
        next();
    }),
    raw(async (req: Request, res: Response) => {
        const playlist = await updatePlaylist(req.params.id, req.body);
        res.status(200).json(playlist);
    })
);

// DELETES A PLAYLIST
router.delete(
    "/:id",
    raw(async (req: Request, res: Response) => {
        const playlist = await deletePlaylist(req.params.id);
        if (!playlist)
            return res.status(404).json({ status: "No playlist found." });
        res.status(200).json(playlist);
    })
);

// GETS Pagination
router.get(
    "/paginate/:pageNum/:nPerPage",
    raw(async (req: Request, res: Response) => {
        const playlist = await getPaginate(
            req.params.pageNum,
            req.params.nPerPage
        );
        res.status(200).json(playlist);
    })
);

export default router;
