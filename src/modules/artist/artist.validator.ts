import Joi from "joi";
const schema = Joi.object({
    first_name: Joi.string().min(2).max(30).required(),
    last_name: Joi.string().min(2).max(30).required(),
    songs: Joi.array(),
});
export default schema;
