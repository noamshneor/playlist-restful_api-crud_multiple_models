/* 
  if there is an error thrown in the DB, asyncMiddleware
  will pass it to next() and express will handle the error */
import raw from "../../middleware/route.async.wrapper.js";
import express, { Request, Response, NextFunction } from "express";
import schema from "./artist.validator.js";
import {
    newArtist,
    getAllArtists,
    getArtist,
    updateArtist,
    deleteArtist,
    getPaginate,
} from "./artist.service.js";

const router = express.Router();

// parse json req.body on post routes
router.use(express.json());

// CREATES A NEW ARTIST
router.post(
    "/",
    raw(async (req: Request, res: Response, next: NextFunction) => {
        await schema.validateAsync(req.body);
        next();
    }),
    raw(async (req: Request, res: Response) => {
        const artist = await newArtist(req.body);
        res.status(200).json(artist);
    })
);

// GET ALL ARTISTS
router.get(
    "/",
    raw(async (req: Request, res: Response) => {
        const artists = await getAllArtists();
        res.status(200).json(artists);
    })
);

// GETS A SINGLE ARTIST
router.get(
    "/:id",
    raw(async (req: Request, res: Response) => {
        const artist = await getArtist(req.params.id);
        if (!artist)
            return res.status(404).json({ status: "No artist found." });
        res.status(200).json(artist);
    })
);

// UPDATES A SINGLE ARTIST
router.put(
    "/:id",
    raw(async (req: Request, res: Response, next: NextFunction) => {
        await schema.validateAsync(req.body);
        next();
    }),
    raw(async (req: Request, res: Response) => {
        const artist = await updateArtist(req.params.id, req.body);
        res.status(200).json(artist);
    })
);

// DELETES A ARTIST
router.delete(
    "/:id",
    raw(async (req: Request, res: Response) => {
        const artist = await deleteArtist(req.params.id);
        if (!artist)
            return res.status(404).json({ status: "No artist found." });
        res.status(200).json(artist);
    })
);

// GETS Pagination
router.get(
    "/paginate/:pageNum/:nPerPage",
    raw(async (req: Request, res: Response) => {
        const artist = await getPaginate(
            req.params.pageNum,
            req.params.nPerPage
        );
        res.status(200).json(artist);
    })
);

export default router;
