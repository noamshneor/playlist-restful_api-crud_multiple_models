import { ISong } from "../song/song.interface.js";

export interface IArtist {
    id?: string;
    first_name: string;
    last_name: string;
    songs?: ISong[];
}
