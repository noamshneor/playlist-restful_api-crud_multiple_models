import mongoose from "mongoose";
const { Schema, model } = mongoose;

const ArtistSchema = new Schema({
    first_name: String,
    last_name: String,
    songs: [{ type: Schema.Types.ObjectId, ref: "song" }],
});

export default model("artist", ArtistSchema);
