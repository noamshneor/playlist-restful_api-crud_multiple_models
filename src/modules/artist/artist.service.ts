import { IArtist } from "./artist.interface.js";
import {
    newArtistDB,
    getAllArtistsDB,
    getArtistDB,
    updateArtistDB,
    deleteArtistDB,
    getPaginateArtistDB,
} from "./artist.repository.js";

export async function newArtist(artist: IArtist) {
    return await newArtistDB(artist);
}

export async function getAllArtists() {
    return await getAllArtistsDB();
}

export async function getArtist(id: string) {
    return await getArtistDB(id);
}

export async function updateArtist(id: string, payload: IArtist) {
    return await updateArtistDB(id, payload);
}

export async function deleteArtist(id: string) {
    return await deleteArtistDB(id);
}

export async function getPaginate(pageNum: string, nPerPage: string) {
    const pNum = parseInt(pageNum);
    const nPPage = parseInt(nPerPage);
    return await getPaginateArtistDB(pNum, nPPage);
}

export function generateID() {
    return Math.random().toString(36).substring(2);
}
