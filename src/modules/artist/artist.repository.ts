import artist_model from "./artist.model.js";
import song_model from "../song/song.model.js";
import { IArtist } from "./artist.interface.js";

export async function newArtistDB(artist: IArtist) {
    return await artist_model.create(artist);
}

export async function getAllArtistsDB() {
    return await artist_model.find().select();
}

export async function getArtistDB(id: string) {
    return await artist_model.findById(id);
}

export async function updateArtistDB(id: string, payload: IArtist) {
    return await artist_model.findByIdAndUpdate(id, payload, {
        new: true,
        upsert: false,
    });
}

export async function deleteArtistDB(id: string) {
    const artist = await artist_model.findByIdAndRemove(id);
    artist.songs.forEach(async (song: string) => {
        await song_model.findByIdAndRemove(song);
    });
    return artist;
}

export async function getPaginateArtistDB(pageNum: number, nPerPage: number) {
    return await artist_model
        .find()
        .skip(pageNum > 0 ? (pageNum - 1) * nPerPage : 0)
        .limit(nPerPage)
        .select();
}
