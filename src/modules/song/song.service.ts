import {
    newSongDB,
    getAllSongsDB,
    getArtistSongsDB,
    getSongDB,
    updateSongDB,
    deleteSongDB,
    getPaginateSongDB,
} from "../song/song.repository.js";
import { ISong } from "./song.interface.js";

// post - /api/songs/
export async function newSong(song: ISong) {
    return await newSongDB(song);
}

// get - /api/songs/
export async function getAllSongs() {
    return await getAllSongsDB();
}

// get - /api/songs/artist/:id
export async function getArtistSongs(id: string) {
    return await getArtistSongsDB(id);
}

// get - /api/songs/:id
export async function getSong(id: string) {
    return await getSongDB(id);
}

// put - /api/songs/:id
export async function updateSong(id: string, payload: ISong) {
    return await updateSongDB(id, payload);
}

// delete - /api/songs/:id
export async function deleteSong(id: string) {
    return await deleteSongDB(id);
}

// get - /api/songs/paginate/:pageNum/:nPerPage
export async function getPaginate(pageNum: string, nPerPage: string) {
    const pNum = parseInt(pageNum);
    const nPPage = parseInt(nPerPage);
    return await getPaginateSongDB(pNum, nPPage);
}
