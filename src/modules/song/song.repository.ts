import song_model from "./song.model.js";
import artist_model from "../artist/artist.model.js";
import playlist_model from "../playlist/playlist.model.js";
import { ISong } from "./song.interface.js";

export async function newSongDB(payload: ISong) {
    const song = await song_model.create(payload);
    const artist = await artist_model.findById(payload.artist);
    artist.songs.push(song);
    await artist.save();
    return song;
}

export async function getAllSongsDB() {
    return await song_model.find().populate("artist");
}

export async function getArtistSongsDB(id: string) {
    return await artist_model.findById(id);
}

export async function getSongDB(id: string) {
    return await song_model.findById(id);
}

export async function updateSongDB(id: string, payload: ISong) {
    return await song_model.findByIdAndUpdate(id, payload, {
        new: true,
        upsert: false,
    });
}

export async function deleteSongDB(id: string) {
    const song = await song_model.findByIdAndRemove(id);
    const artist = await artist_model.findById(song.artist);
    const index = artist.songs.indexOf(song.id);
    if (index > -1) {
        artist.songs.splice(index, 1);
    }
    await artist.save();

    for (const playlistID of song.playlists) {
        const playlist = await playlist_model.findById(playlistID);
        const index = playlist.songs.indexOf(song.id);
        if (index > -1) {
            playlist.songs.splice(index, 1);
        }
        await playlist.save();
    }

    return song;
}

export async function getPaginateSongDB(pageNum: number, nPerPage: number) {
    return await song_model
        .find()
        .skip(pageNum > 0 ? (pageNum - 1) * nPerPage : 0)
        .limit(nPerPage)
        .select();
}
