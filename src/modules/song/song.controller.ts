/* 
  if there is an error thrown in the DB, asyncMiddleware
  will pass it to next() and express will handle the error */
import raw from "../../middleware/route.async.wrapper.js";
import express, { Request, Response, NextFunction } from "express";
import schema from "./song.validator.js";
import {
    newSong,
    getAllSongs,
    getArtistSongs,
    getSong,
    updateSong,
    deleteSong,
    getPaginate,
} from "./song.service.js";

const router = express.Router();

// parse json req.body on post routes
router.use(express.json());

// CREATES A NEW SONG
router.post(
    "/",
    raw(async (req: Request, res: Response, next: NextFunction) => {
        await schema.validateAsync(req.body);
        next();
    }),
    raw(async (req: Request, res: Response) => {
        const song = await newSong(req.body);
        res.status(200).json(song);
    })
);

// GET ALL SONGS
router.get(
    "/",
    raw(async (req: Request, res: Response) => {
        const songs = await getAllSongs();
        res.status(200).json(songs);
    })
);

// GET ALL SONGS OF A ARTIST
router.get(
    "/artist/:id",
    raw(async (req, res) => {
        const songs = await getArtistSongs(req.params.id);
        res.status(200).json(songs);
    })
);

// GETS A SINGLE SONG
router.get(
    "/:id",
    raw(async (req: Request, res: Response) => {
        const song = await getSong(req.params.id);
        if (!song) return res.status(404).json({ status: "No song found." });
        res.status(200).json(song);
    })
);

// UPDATES A SINGLE SONG
router.put(
    "/:id",
    raw(async (req: Request, res: Response, next: NextFunction) => {
        await schema.validateAsync(req.body);
        next();
    }),
    raw(async (req: Request, res: Response) => {
        const song = await updateSong(req.params.id, req.body);
        res.status(200).json(song);
    })
);

// DELETES A SONG
router.delete(
    "/:id",
    raw(async (req: Request, res: Response) => {
        const song = await deleteSong(req.params.id);
        if (!song) return res.status(404).json({ status: "No song found." });
        res.status(200).json(song);
    })
);

// GETS Pagination
router.get(
    "/paginate/:pageNum/:nPerPage",
    raw(async (req: Request, res: Response) => {
        const songs = await getPaginate(
            req.params.pageNum,
            req.params.nPerPage
        );
        res.status(200).json(songs);
    })
);

export default router;
