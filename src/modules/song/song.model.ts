import mongoose from "mongoose";
const { Schema, model } = mongoose;

const SongSchema = new Schema({
    title: String,
    artist: { type: Schema.Types.ObjectId, ref: "artist" },
    playlists: [{ type: Schema.Types.ObjectId, ref: "playlist" }],
});

export default model("song", SongSchema);
