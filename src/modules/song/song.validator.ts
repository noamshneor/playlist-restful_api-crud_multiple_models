import Joi from "joi";
const schema = Joi.object({
    title: Joi.string().min(2).max(30).required(),
    artist: Joi.string().required(),
    playlists: Joi.array(),
});
export default schema;
