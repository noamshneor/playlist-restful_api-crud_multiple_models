import { IArtist } from "../artist/artist.interface.js";
import { IPlaylist } from "../playlist/playlist.interface.js";

export interface ISong {
    id?: string;
    title: string;
    artist: IArtist;
    playlists?: IPlaylist[];
}
