import { IPlaylist } from "../playlist/playlist.interface.js";

export interface IUser {
    id?: string;
    email: string;
    password: string;
    playlists?: IPlaylist[];
    refresh_token?: string|null;
}
