import Joi from "joi";
const schema = Joi.object({
    email: Joi.string()
        .email({ minDomainSegments: 2, tlds: { allow: ["com", "net"] } })
        .required(),
    password: Joi.string().min(2).max(30).required(),
    playlists: Joi.array(),
    refresh_token: Joi.string(),
});
export default schema;
