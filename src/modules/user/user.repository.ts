import user_model from "./user.model.js";
import playlist_model from "../playlist/playlist.model.js";
import { IUser } from "./user.interface.js";
import { IPlaylist } from "../playlist/playlist.interface.js";

export async function newUserDB(user: IUser): Promise<IUser> {
    const newuser = await user_model.create(user);
    return { ...newuser, ...user };
}

export async function getAllUsersDB(): Promise<IUser[]> {
    return await user_model.find().select();
}

export async function getUserDB(id: string): Promise<IUser> {
    return await user_model.findById(id);
}

export async function getUserEmailDB(email: string): Promise<IUser> {
    return await user_model.findOne({ email });
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export async function updateUserDB(id: string, payload: any): Promise<IUser> {
    return await user_model.findByIdAndUpdate(id, payload, {
        new: true,
        upsert: false,
    });
}

export async function deleteUserDB(id: string): Promise<IUser> {
    const user: IUser = await user_model.findByIdAndRemove(id);
    if (user.playlists) {
        user.playlists.forEach(async (playlist: IPlaylist) => {
            await playlist_model.findByIdAndRemove(playlist);
        });
    }
    return user;
}

export async function getPaginateUserDB(pageNum: number, nPerPage: number) {
    return await user_model
        .find()
        .skip(pageNum > 0 ? (pageNum - 1) * nPerPage : 0)
        .limit(nPerPage)
        .select();
}
