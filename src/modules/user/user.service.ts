import { Request } from "express";
import { IUser } from "./user.interface.js";
import {
    newUserDB,
    getAllUsersDB,
    getUserDB,
    updateUserDB,
    deleteUserDB,
    getPaginateUserDB,
} from "./user.repository";

export async function newUser(user: IUser) {
    return await newUserDB(user);
}

export async function getAllUsers() {
    return await getAllUsersDB();
}

export async function getUser(id: string) {
    return await getUserDB(id);
}

export async function updateUser(id: string, payload: IUser) {
    return await updateUserDB(id, payload);
}

export async function deleteUser(id: string) {
    return await deleteUserDB(id);
}

export async function getPaginate(req: Request) {
    const pageNum = parseInt(req.params.pageNum);
    const nPerPage = parseInt(req.params.nPerPage);
    return await getPaginateUserDB(pageNum, nPerPage);
}
