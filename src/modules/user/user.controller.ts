/* 
  if there is an error thrown in the DB, asyncMiddleware
  will pass it to next() and express will handle the error */
import raw from "../../middleware/route.async.wrapper.js";
import express, { Request, Response, NextFunction } from "express";
import schema from "./user.validator.js";
import {
    newUser,
    getAllUsers,
    getUser,
    updateUser,
    deleteUser,
    getPaginate,
} from "./user.service.js";

const router = express.Router();

// parse json req.body on post routes
router.use(express.json());

// CREATES A NEW USER
router.post(
    "/",
    raw(async (req: Request, res: Response, next: NextFunction) => {
        await schema.validateAsync(req.body);
        next();
    }),
    raw(async (req: Request, res: Response) => {
        const user = await newUser({
            email: req.body.email,
            password: req.body.password,
        });
        res.status(200).json(user);
    })
);

// GET ALL USERS
router.get(
    "/",
    raw(async (req: Request, res: Response) => {
        const users = await getAllUsers();
        res.status(200).json(users);
    })
);

// GETS A SINGLE USER
router.get(
    "/:id",
    raw(async (req: Request, res: Response) => {
        const user = await getUser(req.params.id);
        if (!user) return res.status(404).json({ status: "No user found." });
        res.status(200).json(user);
    })
);

// UPDATES A SINGLE USER
router.put(
    "/:id",
    raw(async (req: Request, res: Response, next: NextFunction) => {
        await schema.validateAsync(req.body);
        next();
    }),
    raw(async (req: Request, res: Response) => {
        const user = await updateUser(req.params.id, req.body);
        res.status(200).json(user);
    })
);

// DELETES A USER
router.delete(
    "/:id",
    raw(async (req: Request, res: Response) => {
        const user = await deleteUser(req.params.id);
        if (!user) return res.status(404).json({ status: "No user found." });
        res.status(200).json(user);
    })
);

// GETS Pagination
router.get(
    "/paginate/:pageNum/:nPerPage",
    raw(async (req: Request, res: Response) => {
        const user = await getPaginate(req);
        res.status(200).json(user);
    })
);

export default router;
