import mongoose from "mongoose";
const { Schema, model } = mongoose;

const UserSchema = new Schema({
    email: String,
    password: String,
    playlists: [{ type: Schema.Types.ObjectId, ref: "playlist" }],
    refresh_token: String,
});

export default model("user", UserSchema);
