// require('dotenv').config();
import express from "express";
import morgan from "morgan";
import log from "@ajar/marker";
import cors from "cors";

import { connect_db } from "./db/mongoose.connection.js";
import artist_controller from "./modules/artist/artist.controller.js";
import song_controller from "./modules/song/song.controller.js";
import playlist_controller from "./modules/playlist/playlist.controller.js";
import auth_router from "./auth/auth.router.js";

export const pathHttpLOG = "./http.log";
export const pathErrorLOG = "./error.log";

import {
    error_handler_log,
    error_handler_status,
    error_handler_res,
    not_found,
} from "./middleware/errors.handler.js";
import { setID, getLogs, httpLogger } from "./middleware/log.middleware.js";

class App {
    PORT: number;
    HOST: string;
    DB_URI: string;
    app: express.Application;

    constructor() {
        this.PORT = Number(process.env.PORT) || 8080;
        this.HOST = process.env.HOST || "localhost";
        this.DB_URI = process.env.DB_URI || "";
        this.app = express();
        // middleware
        this.app.use(setID);
        this.app.use(getLogs);
        this.app.use(cors());
        this.app.use(morgan("dev"));

        this.app.use(express.json());

        // routing
        this.app.use("/api/auth", auth_router);
        this.app.use("/api/artists", httpLogger, artist_controller);
        this.app.use("/api/songs", httpLogger, song_controller);
        this.app.use("/api/playlists", httpLogger, playlist_controller);
        //when no routes were matched...
        this.app.use("*", not_found); //not found router

        // central error handling
        this.app.use(error_handler_status);
        this.app.use(error_handler_log);
        this.app.use(error_handler_res);

        //start the express api server
        this.startServer();
    }
    async startServer() {
        try {
            console.log(this.DB_URI);

            //connect to mongo db
            await connect_db(this.DB_URI as string);
            await this.app.listen(Number(this.PORT), this.HOST as string);
            log.magenta(
                `api is live on,
                ✨ ⚡  http://${this.HOST}:${this.PORT} ✨ ⚡`
            );
        } catch (err) {
            console.log(err);
        }
    }
}

new App();
