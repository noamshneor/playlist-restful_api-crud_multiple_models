import { Request, Response } from "express";
import jwt from "jsonwebtoken";
import * as userService from "../auth/auth.service.js";
import ms from "ms";
import { getUserDB } from "../modules/user/user.repository.js";

const { APP_SECRET, ACCESS_TOKEN_EXPIRATION, REFRESH_TOKEN_EXPIRATION } =
    process.env;

// login, signup, logout, get-access-token

export const getAccessToken = async (req: Request, res: Response) => {
    //get refresh_token from client - req.cookies
    const { refresh_token } = req.cookies;
    console.log({ refresh_token });

    if (!refresh_token)
        return res.status(401).json({
            status: "Unauthorized",
            payload: "No refresh_token provided.",
        });

    try {
        // verifies secret and checks expiration
        const decoded = await jwt.verify(refresh_token, APP_SECRET as string); //expired date valid? & if i sign the token ? (app secret)
        console.log({ decoded }); // id - user (obj {id})

        //check user refresh token in DB
        const { id } = decoded as jwt.JwtPayload;
        const user = await getUserDB(id);
        if (!user || user.refresh_token !== refresh_token) {
            return res.status(401).json({
                status: "Unauthorized",
                payload: "Unauthorized - Failed to AUTH user",
            });
        }

        const access_token = jwt.sign(
            { id, some: "other value" },
            APP_SECRET as string,
            {
                expiresIn: ACCESS_TOKEN_EXPIRATION, //expires in 1 minute
            }
        );
        res.status(200).json({ access_token });
    } catch (err) {
        console.log("error: ", err);
        return res.status(401).json({
            status: "Unauthorized",
            payload: "Unauthorized - Failed to verify refresh_token.",
        });
    }
};

export const logIn = async (req: Request, res: Response) => {
    const tokens = await userService.logIn(req.body.email, req.body.password);
    if (!tokens) {
        return res.status(401).json({
            status: "Unauthorized",
            payload: "Unauthorized - Failed to verify refresh_token.",
        });
    }
    res.status(200)
        .cookie("refresh_token", tokens.refresh_token, {
            maxAge: ms(REFRESH_TOKEN_EXPIRATION as string),
            httpOnly: true,
        })
        .json({ access_token: tokens.access_token });
};

export const logOut = async (req: Request, res: Response) => {
    await userService.logOut(req.user_id);
    res.status(200)
        .clearCookie("refresh_token")
        .json({ message: "YOU LOGED OUT" });
};

export const signUp = async (req: Request, res: Response) => {
    const tokens = await userService.singUp(req.body.email, req.body.password);
    if (!tokens) {
        return res.status(409).json({
            status: "Conflict",
            payload: "Conflict - User already exists",
        });
    }
    res.status(200)
        .cookie("refresh_token", tokens.refresh_token, {
            maxAge: ms(REFRESH_TOKEN_EXPIRATION as string),
            httpOnly: true,
        })
        .json({ access_token: tokens.access_token });
};
