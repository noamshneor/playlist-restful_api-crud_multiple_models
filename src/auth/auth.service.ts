import {
    getUserEmailDB,
    newUserDB,
    updateUserDB,
} from "../modules/user/user.repository.js";
import jwt from "jsonwebtoken";
import bcrypt from "bcryptjs";
import { IUser } from "../modules/user/user.interface.js";

const { APP_SECRET, ACCESS_TOKEN_EXPIRATION, REFRESH_TOKEN_EXPIRATION } =
    process.env;

export async function singUp(email: string, pw: string) {
    const existingUser = await getUserEmailDB(email);
    if (existingUser) {
        return null;
    }
    const salt = await bcrypt.genSalt();
    const hashPW = await bcrypt.hash(pw, salt);
    const newUser = await newUserDB({ email, password: hashPW });

    const { refresh_token, access_token } = generateTokens(newUser);

    await updateUserDB(newUser.id as string, { ...newUser, refresh_token });

    return { refresh_token, access_token };
}

export async function logIn(email: string, pw: string) {
    const user = await getUserEmailDB(email);
    if (!user || !(await bcrypt.compare(pw, user.password))) {
        return null;
    }

    const { refresh_token, access_token } = generateTokens(user);

    await updateUserDB(user.id as string, { refresh_token });

    return { refresh_token, access_token };
}

export async function logOut(id: string) {
    await updateUserDB(id, { refresh_token: null });
}

export function generateTokens(user: IUser) {
    const refresh_token = jwt.sign(
        { id: user.id, some: "other value" },
        APP_SECRET as string,
        {
            expiresIn: REFRESH_TOKEN_EXPIRATION, //expires in 60 d
        }
    );

    const access_token = jwt.sign(
        { id: user.id, some: "other value" },
        APP_SECRET as string,
        {
            expiresIn: ACCESS_TOKEN_EXPIRATION, //expires in 1 minute
        }
    );
    return { refresh_token, access_token };
}
