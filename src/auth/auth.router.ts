import express from "express";
import cookieParser from "cookie-parser";
import { getAccessToken, signUp, logIn, logOut } from "./auth.controller.js";
import raw from "../middleware/route.async.wrapper.js";
import { verifyAuth } from "../middleware/auth.middlewate.js";

const router = express.Router();
router.use(cookieParser());

router.get("/get-access-token", getAccessToken);
router.post("/signup", raw(signUp));
router.post("/login", raw(logIn));
router.post("/logout", verifyAuth, raw(logOut));

export default router;
